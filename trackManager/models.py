# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Track(models.Model):
	name=models.CharField(null=False,max_length=200)	

	class Meta:
		verbose_name = u'Track'
		verbose_name_plural = u'Tracks'

	def __unicode__(self):
		return self.name


class Choice(models.Model):
	name=models.TextField(null=False)

	class Meta:
		verbose_name = u'Choice'
		verbose_name_plural = u'Choices'

	def __unicode__(self):
		return self.name


class Question(models.Model):
	track=models.ForeignKey(Track,null=False)	
	choices=models.ManyToManyField(Choice,blank=True,related_name='all_question_choices')
	correctChoice=models.ForeignKey(Choice,null=False,related_name='correctCoice')

	class Meta:
		verbose_name = u'Question'
		verbose_name_plural = u'Questions'

	def __unicode__(self):
		return self.track.name + ' :: ' + self.correctChoice.name

	def trackName(self):
		return self.track.name

	def choiceName(self):
		return self.correctChoice.name

class Student(models.Model):
    name = models.CharField(null=False, max_length=50)
    email = models.CharField(null=True, max_length=100,db_index=True,unique=True)
    phone = models.CharField(null=False, max_length=20)
    created=models.DateTimeField(auto_now_add=True)
    class Meta:
        verbose_name = u'Student'
        verbose_name_plural = u'Students'

    def __unicode__(self):
        return str(self.id)+ ' :: ' +self.name

class Answer(models.Model):
	user=models.ForeignKey(Student,null=True, blank=True)	
	question=models.ForeignKey(Question,null=False)
	selectChoice=models.ForeignKey(Choice,null=False)
	isCorrect=models.BooleanField(default=False)

	class Meta:
		verbose_name = u'Answer'
		verbose_name_plural = u'Answers'

	def __unicode__(self):
		return str( self.id)+ ' :: ' + self.selectChoice.name

	def userName(self):
		return self.track.name

	def choiceName(self):
		return self.selectChoice.name