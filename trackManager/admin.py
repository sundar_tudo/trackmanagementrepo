# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.contrib.admin import AdminSite
from .models import Track,Choice,Question,Student,Answer

# Register your models here.

class trackManagementAdmin(AdminSite):
    site_header = 'Track Management Admin Interface'
    site_title = 'Track Management Administration'
    index_title = 'Track Management Administration'
    site_url = None

class trackDetails(admin.ModelAdmin):
	list_display=("id","name")

class choiceDetails(admin.ModelAdmin):
	list_display=("id","name")

class questionDetails(admin.ModelAdmin):
	list_display=("id","trackName","choiceName")

class studentDetails(admin.ModelAdmin):
	list_display=("id","name","email","phone")

class answerDetails(admin.ModelAdmin):
	list_display=("id","userName","choiceName")


admin_site = trackManagementAdmin(name='superadmin')
admin_site.register(Track,trackDetails)
admin_site.register(Choice,choiceDetails)
admin_site.register(Question,questionDetails)
admin_site.register(Student,studentDetails)
admin_site.register(Answer,answerDetails)
