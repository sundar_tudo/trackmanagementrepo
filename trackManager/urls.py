from django.conf.urls import url

from . import views

urlpatterns = [
     url(r'createTrack/$', views.createTrack,name='createTrack'),
     url(r'updateTrack/$', views.updateTrack,name='updateTrack'),
     url(r'getAllTracks/$', views.getAllTracks,name='getAllTracks'),
     url(r'deleteTrack/$', views.deleteTrack,name='deleteTrack'),
     url(r'getListOfQuestionForTrack/$', views.getListOfQuestionForTrack,name='getListOfQuestionForTrack'),
     url(r'coutnTotalCorrectAndWrongAnswerForQuestion/$', views.coutnTotalCorrectAndWrongAnswerForQuestion,name='coutnTotalCorrectAndWrongAnswerForQuestion')
    ]