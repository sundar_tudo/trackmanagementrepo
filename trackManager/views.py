# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
import sys
import traceback
import json
from trackManagement import settings
from django.core.exceptions import ObjectDoesNotExist
from urllib import urlencode
from .models import Track,Choice,Question,Student,Answer

# Create your views here.


@csrf_exempt
def createTrack(request):
	output={}
	data=json.loads(request.body)
	try:
		name=data['name']
		try:
			trackObj=Track.objects.create(name=name)
			output['status']="success"
			output['status_text']="track created sucessfully"
			output['trackData']=jsonOfTrackObject(trackObj)
		except:
			traceback.print_exc(file=sys.stdout)
			output['status']="failed"
			output['status_text']="failed to create track please try again.."
	except KeyError as e:
		traceback.print_exc(file=sys.stdout)
		output['status']="Failed"
		output['status_text']="Key "+str(e[0])+" Not Specified"
	except ObjectDoesNotExist:
		traceback.print_exc(file=sys.stdout)
		output['status']="failed"
		output['status_text']="ObjectDoesNotExist "
	except Exception as e:
		traceback.print_exc(file=sys.stdout)
		output['status']='failed'
		output['status_text']=str(e[0])
	return HttpResponse(json.dumps(output))



def jsonOfTrackObject(track):
	trackDetails={}
	trackDetails['id']=track.id
	trackDetails['name']=track.name
	return trackDetails


def jsonOfChoiceObject(choice):
	choiceDetails={}
	choiceDetails['id']=choice.id
	choiceDetails['name']=choice.name
	return choiceDetails

def jsonOfQuestionObject(question):
	questionDetails={}
	questionDetails['track']=jsonOfTrackObject(question.track)
	questionDetails['choices']=[]
	if(len(question.choices.all())>0):
		allChoices=question.choices.all()
		for choice in allChoices:
			questionDetails['choices'].append(jsonOfChoiceObject(choice))
	questionDetails['correctChoice']=jsonOfChoiceObject(question.correctChoice)
	return questionDetails




@csrf_exempt
def updateTrack(request):
	output={}
	data=json.loads(request.body)
	try:
		trackId=data['id']
		trackObj=Track.objects.get(id=trackId)
		try:
			trackObj.name=data['name']
		except KeyError as e:
			traceback.print_exc(file=sys.stdout)
		trackObj.save()
		output['status']="success"
		output['status_text']="track update successfully"
		output['trackData']=jsonOfTrackObject(trackObj)
	except KeyError as e:
		traceback.print_exc(file=sys.stdout)
		output['status']="Failed"
		output['status_text']="Key "+str(e[0])+" Not Specified"
	except ObjectDoesNotExist:
		traceback.print_exc(file=sys.stdout)
		output['status']="failed"
		output['status_text']="ObjectDoesNotExist "
	except Exception as e:
		traceback.print_exc(file=sys.stdout)
		output['status']='failed'
		output['status_text']=str(e[0])
	return HttpResponse(json.dumps(output))


@csrf_exempt
def getAllTracks(request):
	output={}
	try:
		output['trackData']=[]
		trackArray=Track.objects.all().order_by('-id')
		for track in trackArray:
			output['trackData'].append(jsonOfTrackObject(track))
		output['status']="success"
		output['status_text']="successfully get all tracks"
		return HttpResponse(json.dumps(output))
	except:
		traceback.print_exc(file=sys.stdout)
		output['status']="failed"
		output['status_text']="Some Error Occured please try again"
		return HttpResponse(json.dumps(output))



@csrf_exempt
def deleteTrack(request):
	output={}
	data=json.loads(request.body)
	try:
		trackId=data['id']
		trackObj=Track.objects.get(id=trackId)
		trackObj.delete()
		output['status']="success"
		output['status_text']="Deleted!"
	except KeyError as e:
		traceback.print_exc(file=sys.stdout)
		output['status']="Failed"
		output['status_text']="Key "+str(e[0])+" Not Specified"
	except ObjectDoesNotExist:
		traceback.print_exc(file=sys.stdout)
		output['status']="failed"
		output['status_text']="ObjectDoesNotExist "
	except Exception as e:
		traceback.print_exc(file=sys.stdout)
		output['status']='failed'
		output['status_text']=str(e[0])
	return HttpResponse(json.dumps(output))





@csrf_exempt
def getListOfQuestionForTrack(request):
	output={}
	data=json.loads(request.body)
	try:
		trackId=data['id']
		try:
			trackObj=Track.objects.get(id=trackId)
			output['listOfQuestions']=[]
			questionArray=Question.objects.filter(track=trackObj).order_by('-id')
			for question in questionArray:
				output['listOfQuestions'].append(jsonOfQuestionObject(question))
			output['status']="success"
			output['status_text']="successfully get all question for track"
		except KeyError as e:
			traceback.print_exc(file=sys.stdout)
			output['status']="Failed"
			output['status_text']="Key "+str(e[0])+" Not Specified"
	except ObjectDoesNotExist:
		traceback.print_exc(file=sys.stdout)
		output['status']="failed"
		output['status_text']="ObjectDoesNotExist "
	except Exception as e:
		traceback.print_exc(file=sys.stdout)
		output['status']='failed'
		output['status_text']=str(e[0])
	return HttpResponse(json.dumps(output))




@csrf_exempt
def coutnTotalCorrectAndWrongAnswerForQuestion(request):
	output={}
	data=json.loads(request.body)
	try:
		quesId=data['id']
		try:
			questionObj=Question.objects.get(id=quesId)
			totalCorrectAnswer=len(Answer.objects.filter(question=questionObj,isCorrect=True))
			totalWrongAnswer=len(Answer.objects.filter(question=questionObj,isCorrect=False))
			output['totalCorrectAnswer']=totalCorrectAnswer
			output['totalWrongAnswer']=totalWrongAnswer
			output['status']="success"
		except KeyError as e:
			traceback.print_exc(file=sys.stdout)
			output['status']="Failed"
			output['status_text']="Key "+str(e[0])+" Not Specified"
	except ObjectDoesNotExist:
		traceback.print_exc(file=sys.stdout)
		output['status']="failed"
		output['status_text']="ObjectDoesNotExist "
	except Exception as e:
		traceback.print_exc(file=sys.stdout)
		output['status']='failed'
		output['status_text']=str(e[0])
	return HttpResponse(json.dumps(output))